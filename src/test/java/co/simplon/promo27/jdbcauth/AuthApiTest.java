package co.simplon.promo27.jdbcauth;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import co.simplon.promo27.jdbcauth.entity.User;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@Sql({"/database.sql"})
@AutoConfigureMockMvc
public class AuthApiTest {
    @Autowired
    MockMvc mvc;

    @Test
    void shouldPersistUserAndHashPassword() throws Exception {
        mvc.perform(post("/api/user")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "email":"bloup@test.com",
                "password":"1234"
            }
        """))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id").exists())
        .andExpect(jsonPath("$.role").value("ROLE_USER"))
        ;
    }

    @Test
    void shouldNotPersistSameUser() throws Exception {
        mvc.perform(post("/api/user")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "email":"test@test.com",
                "password":"1234"
            }
        """))
        .andExpect(status().isBadRequest())
        ;
    }

    @Test
    void shouldForddibAccessIfNotConnected() throws Exception {
        mvc.perform(get("/api/account"))
        .andExpect(status().isUnauthorized());
    }


    @Test
    @WithUserDetails("test@test.com")
    void shouldReturnTheConnectedUser() throws Exception {
        mvc.perform(get("/api/account"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(2))
        .andExpect(jsonPath("$.email").value("test@test.com"))
        ;
    }
}
