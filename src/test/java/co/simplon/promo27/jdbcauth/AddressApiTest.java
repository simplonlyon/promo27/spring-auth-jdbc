package co.simplon.promo27.jdbcauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;


@SpringBootTest
@Sql({"/database.sql"})
@AutoConfigureMockMvc
public class AddressApiTest {
    @Autowired
    MockMvc mvc;

    @Test
    void shouldReturnAListOfAddresses() throws Exception {
        mvc.perform(get("/api/address"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].id").isNumber())
        .andExpect(jsonPath("$[0].street").isString())
        ;
    }

    @Test
    @WithUserDetails("test@test.com")
    void shouldForbidDeleteIfAddressNotOwned() throws Exception  {
        mvc.perform(delete("/api/address/1"))
        .andExpect(status().isForbidden());
    }

    @Test
    @WithUserDetails("test@test.com")
    void shouldDeleteAddress() throws Exception  {
        mvc.perform(delete("/api/address/2"))
        .andExpect(status().isNoContent());
    }


    @Test
    @WithUserDetails("test@test.com") // c'est vachement bien ça quand même
    void shouldPersistNewAddressForUser() throws Exception  {
        mvc.perform(post("/api/address")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "street":"street test",
                "city":"Villeurbanne"
            }
        """))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id").value(4))
        .andExpect(jsonPath("$.owner.email").value("test@test.com"));
    }


    @Test
    @WithUserDetails("test@test.com")
    void shouldNotPersistWithoutStreet() throws Exception  {
        mvc.perform(post("/api/address")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "city":"Villeurbanne"
            }
        """))
        .andExpect(status().isBadRequest());
    }
}
