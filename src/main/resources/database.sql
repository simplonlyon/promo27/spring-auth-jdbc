-- Active: 1695817678749@@127.0.0.1@3306@p27_auth_jdbc

DROP TABLE IF EXISTS address;
DROP TABLE IF EXISTS user;

CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE, -- Pas obligatoire de mettre UNIQUE, pasque le check sera aussi fait par Spring
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
);

CREATE TABLE address(
    id INT PRIMARY KEY AUTO_INCREMENT,
    street VARCHAR(255),
    city VARCHAR(255),
    id_user INT,
    FOREIGN KEY (id_user) REFERENCES user(id)
);

INSERT INTO user (email,password,role) VALUES 
('admin@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_ADMIN'),
('test@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER');

INSERT INTO address (street,city,id_user) VALUES 
('34 rue antoine primat', 'villeurbanne', 1),
('32 rue antoine primat', 'villeurbanne', 2),
('12 avenue de la république', 'paris', 1);
