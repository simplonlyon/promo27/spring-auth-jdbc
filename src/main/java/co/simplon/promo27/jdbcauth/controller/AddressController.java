package co.simplon.promo27.jdbcauth.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo27.jdbcauth.entity.Address;
import co.simplon.promo27.jdbcauth.entity.User;
import co.simplon.promo27.jdbcauth.repository.AddressRepository;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/address")
public class AddressController {

    @Autowired
    private AddressRepository repo;

    @GetMapping
    public List<Address> all() {
        return repo.findAll();
    }

    @GetMapping("/user/{idUser}")
    public List<Address> byUser(@PathVariable int idUSer) {
        return repo.findByUser(idUSer);
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Address add(@RequestBody @Valid Address address, @AuthenticationPrincipal User user) {
        address.setOwner(user);
        repo.persist(address);
        return address;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id, @AuthenticationPrincipal User user) {
        Address address = repo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        //Si on est pas admin et que l'address ne nous appartient pas, on renvoie une erreur
        if(!user.getRole().equals("ROLE_ADMIN") && !address.getOwner().getId().equals(user.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        repo.delete(id);
    }


}
