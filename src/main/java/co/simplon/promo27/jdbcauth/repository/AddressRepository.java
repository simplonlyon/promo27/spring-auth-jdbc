package co.simplon.promo27.jdbcauth.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo27.jdbcauth.entity.Address;
import co.simplon.promo27.jdbcauth.entity.User;

@Repository
public class AddressRepository {

    @Autowired
    private DataSource dataSource;
    
    public List<Address> findAll() {
        List<Address> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM address LEFT JOIN user ON address.id_user=user.id");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(sqlToAddress(result));
            }

        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return list;
    }

    public List<Address> findByUser(int idUser) {
        List<Address> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM address LEFT JOIN user ON address.id_user=user.id WHERE id_user=?");
            stmt.setInt(1, idUser);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(sqlToAddress(result));
            }

        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return list;
    }

    public Optional<Address> findById(int id) {
        
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM address LEFT JOIN user ON address.id_user=user.id WHERE address.id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return Optional.of(sqlToAddress(result));
            }

        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }


    public boolean delete(int id) {
        
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM address WHERE address.id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        
    }

    public boolean persist(Address address) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO address (street,city,id_user) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, address.getStreet());
            stmt.setString(2, address.getCity());
            stmt.setInt(3, address.getOwner().getId());
            
            if(stmt.executeUpdate() == 1) {
                ResultSet rs = stmt.getGeneratedKeys();
                if(rs.next()) {
                    address.setId(rs.getInt(1));
                    return true;
                }
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }

        return false;
    }

    private Address sqlToAddress(ResultSet result) throws SQLException {
        Address address = new Address(result.getInt("id"),result.getString("street"), result.getString("city"));
        address.setOwner(new User(result.getInt("id_user"), result.getString("email"), null, result.getString("role")));
        return address;
    }
}
