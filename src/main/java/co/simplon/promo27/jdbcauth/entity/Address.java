package co.simplon.promo27.jdbcauth.entity;

import jakarta.validation.constraints.NotBlank;

public class Address {
    private Integer id;
    @NotBlank
    private String street;
    @NotBlank
    private String city;
    private User owner;
    
    public Address(String street, String city) {
        this.street = street;
        this.city = city;
    }
    public Address(Integer id, String street, String city) {
        this.id = id;
        this.street = street;
        this.city = city;
    }
    public Address() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public User getOwner() {
        return owner;
    }
    public void setOwner(User owner) {
        this.owner = owner;
    }
}
