# Spring Authentication With JDBC

Projet Spring Boot avec de l'authentification et les users stockés en base de données via un repository JDBC à la main.


## Dépendances utilisées dans ce projet : 
* Spring Security (obligatoire si on veut gérer l'authentification avec Spring)
* Spring JDBC et Mysql Driver (pour l'accès à la base de données)
* Spring devtools et Spring Actuator (outils de débug et de dev)
* Spring Validation I/O (pour la validation à l'inscription du user)
* Spring Web (pour faire les routes/contrôleurs)


## Configuration de l'authentification

### Une entité User
Une entité User indique ce qui persistera en base de données, en général on souhaitera un champ faisant office de d'identifiant (l'email c'est pas mal pour ça), un mot de passe, et si notre site à plusieurs rôles possibles, un role.

(Ici la classe s'appelle User, mais elle pourrait s'appeler n'importe comment, Student, Account, Customer, etc.)
```java
public class User implements UserDetails {
    private Integer id;
    @Email
    @NotBlank
    private String email;
    @NotBlank
    @Length(min=4)
    @JsonProperty(access = Access.WRITE_ONLY) //Pour faire en sorte de ne pas envoyer le password en JSON
    private String password;
    private String role;
    
    //...
}
```

Pour être utilisable par Spring Security, l'entité doit obligatoirement implémenter l'interface UserDetails et toutes ses méthodes ([exemple d'implémentation](src/main/java/co/simplon/promo27/jdbcauth/entity/User.java))

### Un Repository pour le User
Le repository pour l'entité User doit notamment permettre de récupérer un User spécifique par son identifiant et de le faire persister en base de données.

Exemple avec un  dans le cas où l'identifiant est l'email :
```java
@Repository
public class UserRepository {
    public Optional<User> findByEmail(String email) {
        //méthode JDBC classique avec PreparedStatement et tout
    }

    public boolean persist(User user) {
       //méthode JDBC classique avec PreparedStatement et tout
    }
}
```

### Un UserDetailsService
Spring Security a également besoin d'une classe qui implémente l'interface UserDetailsService, qui indique à Spring comment récupérer un User au moment de la connexion.

Exemple avec une classe qu'on appelle UserService, qu'on peut appeler comme on le souhaite, et qui utilise notre UserRepository :

```java
@Service
public class UserService implements UserDetailsService{
    @Autowired
    private UserRepository repo;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        return repo.findByEmail(username)
        .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }  
}
```


### Fichier de configuration de Spring Security 
Cette classe, qui devra être annotée par un @Configuration devra contenir plusieurs parties ([configuration complète](src/main/java/co/simplon/promo27/jdbcauth/security/SecurityConfig.java))

#### La définition de l'algorithme de Hash
Indique quel PasswordEncoder sera utilisé dans toute l'application, ce qui permettra de hasher le mot de passe au moment de l'inscription dans le contrôleur, et au moment du login (fait par spring security)
```java
@Bean
PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder(12);
}
```
Le @Bean, indique que c'est cette méthode qui sera utilisée pour récupérer une instance lorsqu'on fera un @Autowired de PasswordEncoder

#### La définition des access control
Les access controls représentent la liste des routes et qui y a accès, faut-il être connecté·e pour y accéder, faut-il avoir un rôle spécifique, etc.
```java
@Bean
SecurityFilterChain authConfig(HttpSecurity http) throws Exception {
    
    http.httpBasic(Customizer.withDefaults())
    //On indique à spring qu'il faudra créer une session ce qu'il ne fait pas par défaut
    .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED))
    .csrf(csrf -> csrf.disable())
    //Configuration des cors, voir en dessous
    .cors(cors -> cors.configurationSource(corsConfigurationSource()))
    //Les access controls
    .authorizeHttpRequests(request -> 
        request
        .requestMatchers("/api/account").authenticated()
        .anyRequest().permitAll()
    );


    return http.build();
}
```
Ici, on utilise le requestMatchers pour indiquer éventuellement la méthode ciblée, la ou les routes, et derrière qui a le droit d'accèder ou non à cette route.

Le anyRequest().permitAll() indique que toutes les routes restantes seront accessibles à tout le monde.

#### Configuration des CORS
Les CORS (Cross Origin Resource Sharing) sont des entêtes à rajouter côté serveur lorsque celui ci doit pouvoir être requêté par du Javascript (même sans CORS, les requêtes via Thunder Client marchent par exemple).

Ici on les configure de manière global en indiquant qu'on autorise toutes les requêtes, toutes les méthodes http et tous les en-têtes http venant de l'url de notre application frontend (ici on a mis une variable alimentée par le [application.properties](src/main/resources/application.properties) qui contient `http://localhost:5173`). On indique aussi le AllowCredentials pour que les requêtes faites par le JS puissent inclure les Cookies de session.

```java
private CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowCredentials(true);
    configuration.setAllowedOrigins(Arrays.asList(frontUrl));
    configuration.setAllowedMethods(Arrays.asList("*"));
    configuration.setAllowedHeaders(Arrays.asList("*"));
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);
    return source;
}
```

### Le contrôleur d'authentification

Ce contrôleur est un RestController classique qui contiendra une route permettant de s'inscrire, dans laquelle il faudra faire en sorte de hasher le mot de passe avant de faire persister le user. ([exemple de contrôleur d'authentification](src/main/java/co/simplon/promo27/jdbcauth/controller/AuthController.java))

Il contiendra également une route définie comme authenticated dans le access control, qui permettra de récupérer le user actuellement connecté et qui sevira de route de login

```java
@GetMapping("/api/account")
public User getAccount(@AuthenticationPrincipal User user) {
    return user;
}
```
